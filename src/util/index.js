let formatDateTime = function (inputTime) {
  var date = new Date(inputTime);
  var y = date.getFullYear();
  var m = date.getMonth() + 1;
  m = m < 10 ? ('0' + m) : m;
  var d = date.getDate();
  d = d < 10 ? ('0' + d) : d;
  var h = date.getHours();
  h = h < 10 ? ('0' + h) : h;
  var minute = date.getMinutes();
  var second = date.getSeconds();
  minute = minute < 10 ? ('0' + minute) : minute;
  second = second < 10 ? ('0' + second) : second;
  return y + '-' + m + '-' + d + ' ' + h + ':' + minute + ':' + second;
};

/**
 * 距现在的已过时间
 * @param {string} startTime 开始日期字符串
 * @returns {string}
 */
let passedTime = function (startTime) {
  let currentTime = Date.parse(new Date()),
    time = currentTime - (new Date(startTime)),
    day = parseInt(time / (1000 * 60 * 60 * 24)),
    hour = parseInt(time / (1000 * 60 * 60)),
    min = parseInt(time / (1000 * 60)),
    month = parseInt(day / 30),
    year = parseInt(month / 12);
  if (year) return year + "年前"
  if (month) return month + "个月前"
  if (day) return day + "天前"
  if (hour) return hour + "小时前"
  if (min) return min + "分钟前"
  else return '刚刚'
}


/**
 * 格式化日期
 * @param {string} dateTime 日期/日期字符串
 * @param {object} fmt 格式
 * @return {string}
 */
let format = function (dateTime, fmt = 'yyyy-MM-dd hh:mm:ss') {
  // ios日期是2011/11/11
  let d;
  if (typeof (dateTime) === 'object') {
    d = dateTime;
  } else {
    d = new Date(dateTime.replace(/\-/g, "/"));
  }
  let o = {
    "M+": d.getMonth() + 1,
    "d+": d.getDate(),
    "h+": d.getHours(),
    "m+": d.getMinutes(),
    "s+": d.getSeconds(),
    "q+": Math.floor((d.getMonth() + 3) / 3), //季度
    "S": d.getMilliseconds()
  };
  if (/(y+)/.test(fmt))
    fmt = fmt.replace(RegExp.$1, (d.getFullYear() + "").substr(4 - RegExp.$1.length));
  for (let k in o)
    if (new RegExp("(" + k + ")").test(fmt))
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
  return fmt;
}

/**
 * 平滑数据转element tree用格式
 * @param data
 * @returns {Array}
 */
let toTreeData = (data) => {
  var idFiled, textFiled, parentField;
  idFiled = 'id';
  textFiled = 'name';
  parentField = 'pid';
  var i, l, treeData = [],
    tmpMap = [];
  for (i = 0, l = data.length; i < l; i++) {
    tmpMap[data[i][idFiled]] = data[i];
  }
  for (i = 0, l = data.length; i < l; i++) {
    if (tmpMap[data[i][parentField]] && data[i][idFiled] != data[i][parentField]) {
      if (!tmpMap[data[i][parentField]]['children'])
        tmpMap[data[i][parentField]]['children'] = [];
      data[i]['label'] = data[i][textFiled];
      tmpMap[data[i][parentField]]['children'].push(data[i]);
    } else {
      data[i]['label'] = data[i][textFiled];
      treeData.push(data[i]);
    }
  }
  return treeData;
}

/**
 * 处理请求参数-转成接口用的形式
 * @param data
 * @returns {{}}
 */
let handleReqParams = function (data) {
  let reqParamsData = {}
  for (let o in data) {
    reqParamsData[data[o].name] = data[o].mockValue
  }
  return reqParamsData;
}

/**
 * 处理请求头参数-转成接口用的形式
 * @param data
 * @returns {{}}
 */
let handleReqHeaders = function (data) {
  let reqHeadersData = {}
  for (let o in data) {
    reqHeadersData[data[o].key_] = data[o].value_
  }
  return reqHeadersData
}

export default {
  formatDateTime,
  passedTime,
  format,
  toTreeData,
  handleReqParams,
  handleReqHeaders
}
