import Vue from 'vue'
import Router from 'vue-router'
import index from '../pages/index.vue'
import preview from '../pages/preview.vue'
import proDetial from '../pages/proDetial.vue'
import login from '../pages/login'

Vue.use(Router)

export default new Router({
  routes: [
    {path: '/', component: index},
    {path: '/proDetial', component: proDetial},
    {path: '/preview', component: preview},
    {path: '/login', component: login},
  ]
})
